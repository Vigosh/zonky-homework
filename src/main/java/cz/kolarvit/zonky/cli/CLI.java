package cz.kolarvit.zonky.cli;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.kolarvit.zonky.service.ExternalZonkyService;
import cz.kolarvit.zonky.service.LoanStorage;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * CLI - Command Line Interface of the application.
 * Included scheduled task for printing new loans on Zonky.
 */
@Log
@Component
public class CLI {

    private ExternalZonkyService zonkyService;
    private LoanStorage loanStorage;

    @Value("${zonky.include.summary:false}")
    private boolean includeSummary;

    @Value("${zonky.pretty.print:false}")
    private boolean prettyPrint;

    @Autowired
    public CLI(ExternalZonkyService zonkyService, LoanStorage loanStorage) {
        this.zonkyService = zonkyService;
        this.loanStorage = loanStorage;
    }

    @Scheduled(initialDelay = 1000, fixedDelayString = "${zonky.check.interval.milis:300000}")
    public void execute() {
        printAppPrologue();
        OffsetDateTime lastLoanDateTime = loanStorage.getLastLoanDateTime();
        List<Map<String, Object>> newLoansMap = zonkyService.getNewLoansMap(lastLoanDateTime);
        loanStorage.storyLoans(newLoansMap);
        printSummary(lastLoanDateTime, newLoansMap);
        printLoans(newLoansMap);
    }

    private void printAppPrologue() {
        if (loanStorage.getLastLoanDateTime() != null) {
            return;
        }
        System.out.println("##############################################################");
        System.out.println("Application \"Zonky new loans printer\"");
        System.out.println("Possible configurations are in application.properties");
        System.out.println("  zonky.include-summary");
        System.out.println("      include summary every refresh (default false)");
        System.out.println("  zonky.pretty.print");
        System.out.println("      pretty JSON output (default false)");
        System.out.println("  zonky.check.interval.milis");
        System.out.println("      refresh interval in milis (default 300000 - 5 minutes)");
        System.out.println("##############################################################");
        System.out.println();
    }

    private void printSummary(OffsetDateTime latestDateTime, List<Map<String, Object>> loans) {
        if (!includeSummary) {
            return;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        System.out.println();
        if (latestDateTime == null) {
            System.out.println("Zonky loans searching");
        } else {
            System.out.println("Zonky loans searching from date: " + latestDateTime.format(formatter));
        }
        System.out.println("Current time: " + LocalDateTime.now().format(formatter));
        System.out.println("Number of found new loans: " + loans.size());
    }

    private void printLoans(List<Map<String, Object>> loans) {
        ObjectMapper mapper = new ObjectMapper();
        loans.forEach(loan -> {
            try {
                if (prettyPrint) {
                    System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(loan));
                } else {
                    System.out.println(loan);
                }
            } catch (JsonProcessingException e) {
                handleException(e);
            }
        });
    }

    private void handleException(Throwable e) {
        System.err.println("An error occurred: " + e.getMessage());
        log.log(Level.SEVERE, "An error occurred: " + e.getMessage(), e);
    }
}
