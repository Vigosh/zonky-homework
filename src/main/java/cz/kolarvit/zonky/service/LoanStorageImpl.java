package cz.kolarvit.zonky.service;

import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class LoanStorageImpl implements LoanStorage {

    private Map<Integer, OffsetDateTime> loanMap = new ConcurrentHashMap<>();

    @Override
    public OffsetDateTime getLastLoanDateTime() {
        if (loanMap.entrySet().iterator().hasNext())
            return loanMap.entrySet().iterator().next().getValue();
        return null;
    }

    @Override
    public void storyLoans(List<Map<String, Object>> newLoansMap) {
        for (Map<String, Object> map : newLoansMap) {
            if (map.containsKey("id") && map.containsKey("datePublished")) {
                addLoan((Integer) map.get("id"),
                        OffsetDateTime.parse((CharSequence) map.get("datePublished"),
                                DateTimeFormatter.ISO_OFFSET_DATE_TIME));
            }
        }
    }

    private void addLoan(Integer id, OffsetDateTime instantDate) {
        loanMap.put(id, instantDate);
        loanMap = sortByValue(loanMap, true);
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map, boolean desc) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());
        if (desc) {
            Collections.reverse(list);
        }
        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
}
