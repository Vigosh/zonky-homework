package cz.kolarvit.zonky.service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

/**
 * Service for counting words for histogram.
 */
public interface LoanStorage {

    /**
     * @return datePublished from last stored record
     */
    OffsetDateTime getLastLoanDateTime();

    /**
     * Store loan records.
     *
     * @param newLoansMap JSON as map
     */
    void storyLoans(List<Map<String, Object>> newLoansMap);
}
