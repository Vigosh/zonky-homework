package cz.kolarvit.zonky.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.kolarvit.zonky.exception.ZonkyException;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

@Service
@Log
public class ExternalZonkyServiceImpl implements ExternalZonkyService {

    @Override
    public List<Map<String, Object>> getNewLoansMap(OffsetDateTime lastLoanDateTime) {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Map<String, Object>>> typeRef = new TypeReference<>() {
        };
        try {
            return mapper.readValue(getNewLoans(lastLoanDateTime), typeRef);
        } catch (IOException e) {
            handleException(e);
            return null;
        }
    }

    @Override
    public String getNewLoans(OffsetDateTime lastLoanDateTime) {
        try {
            HttpClient client = HttpClient.newHttpClient();
            String url = "https://api.zonky.cz/loans/marketplace";
            if (lastLoanDateTime != null) {
                url += "?datePublished__gt=" + URLEncoder.encode(lastLoanDateTime.toString(), StandardCharsets.UTF_8);
            }
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url).normalize())
                    .GET()
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            int statusCode = response.statusCode();
            if (Response.Status.Family.familyOf(statusCode) != Response.Status.Family.SUCCESSFUL) {
                throw new ZonkyException("Response is not successful. Code: " + statusCode);
            }
            return response.body();
        } catch (InterruptedException | IOException e) {
            handleException(e);
        }
        return null;
    }

    private void handleException(Throwable e) {
        System.err.println("An error occurred: " + e.getMessage());
        log.log(Level.SEVERE, "An error occurred: " + e.getMessage(), e);
    }
}
