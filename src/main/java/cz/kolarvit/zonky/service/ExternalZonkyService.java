package cz.kolarvit.zonky.service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

public interface ExternalZonkyService {

    /**
     * Get new loans from Zonky as JSON map.
     */
    List<Map<String, Object>> getNewLoansMap(OffsetDateTime lastLoanDateTime);

    /**
     * Get new loans from Zonky as JSON string.
     */
    String getNewLoans(OffsetDateTime lastLoanDateTime);
}
