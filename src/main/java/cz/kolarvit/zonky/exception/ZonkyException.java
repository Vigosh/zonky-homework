package cz.kolarvit.zonky.exception;

public class ZonkyException extends RuntimeException {

    public ZonkyException() {
    }

    public ZonkyException(String message) {
        super(message);
    }

    public ZonkyException(Throwable cause) {
        super(cause);
    }

    public ZonkyException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZonkyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
