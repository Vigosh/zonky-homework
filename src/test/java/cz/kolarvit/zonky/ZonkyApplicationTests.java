package cz.kolarvit.zonky;

import cz.kolarvit.zonky.cli.CLI;
import cz.kolarvit.zonky.service.ExternalZonkyService;
import cz.kolarvit.zonky.service.LoanStorage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZonkyApplicationTests {

    @Autowired
    private CLI cli;

    @Autowired
    private LoanStorage loanStorage;

    @Autowired
    private ExternalZonkyService externalZonkyService;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(cli);
        Assert.assertNotNull(loanStorage);
        Assert.assertNotNull(externalZonkyService);
    }
}
