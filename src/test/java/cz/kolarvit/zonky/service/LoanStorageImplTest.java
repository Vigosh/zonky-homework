package cz.kolarvit.zonky.service;

import org.junit.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class LoanStorageImplTest {

    /**
     * Test for JSON: [{\"id\":473004,\"datePublished\":\"2019-05-22T06:53:13.271+02:00\",\"published\":true"}]
     */
    @Test
    public void storageTestPass() {
        List<Map<String, Object>> jsonMap = new ArrayList<>();
        addDataForTestPass(jsonMap);
        LoanStorageImpl storage = new LoanStorageImpl();
        storage.storyLoans(jsonMap);

        assertEquals(OffsetDateTime.of(2019, 5, 22, 6, 53, 13, 271000000, ZoneOffset.of("+02:00")),
                storage.getLastLoanDateTime());
    }

    /**
     * Test for JSON: [{"datePublished\":\"2019-05-22T06:53:13.271+02:00\",\"published\":true"}]
     */
    @Test
    public void storageTestFail() {
        List<Map<String, Object>> jsonMap = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("datePublished", "2019-05-22T06:53:13.271+02:00");
        map.put("published", "true");
        jsonMap.add(map);
        LoanStorageImpl storage = new LoanStorageImpl();
        storage.storyLoans(jsonMap);

        assertNull(storage.getLastLoanDateTime());
    }

    private void addDataForTestPass(List<Map<String, Object>> jsonMap) {
        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("id", 473003);
        map1.put("datePublished", "2019-05-21T06:53:13.271+02:00");
        map1.put("published", "true");
        jsonMap.add(map1);

        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("id", 473004);
        map2.put("datePublished", "2019-05-22T06:53:13.271+02:00");
        map2.put("published", "true");
        jsonMap.add(map2);

        HashMap<String, Object> map3 = new HashMap<>();
        map3.put("id", 473005);
        map3.put("datePublished", "2019-05-22T04:40:13.271+02:00");
        map3.put("published", "true");
        jsonMap.add(map3);
    }
}